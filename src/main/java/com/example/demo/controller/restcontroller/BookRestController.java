package com.example.demo.controller.restcontroller;

import com.example.demo.models.Book;
import com.example.demo.models.filter.BookFilter;
import com.example.demo.service.BookService;
import com.example.demo.service.UploadService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/api/v1/book")
/* used to set custom status code message*/
@ApiResponses({
        @ApiResponse(code = 200, message = "Ok custom"),
        @ApiResponse(code = 404, message = "Not found custom")
})
/* used to customize API Doc title for each API*/
@Api(description = "Here is my Book Web Service")
public class BookRestController {

    private BookService bookService;
    private UploadService uploadService;

    @Autowired
    public BookRestController(BookService bookService, UploadService uploadService) {
        this.bookService = bookService;
        this.uploadService = uploadService;
    }

    @GetMapping("/all")
//    @ApiIgnore    /* used to hide API in API Doc*/
    @ApiOperation(value = "Get all books")   /* used to add description to API in API Doc */
    @ResponseBody
    public List<Book> getAll(){
        return this.bookService.getBookList();
    }

    @GetMapping("/filter")
    @ResponseBody
    public List<Book> getFilter(BookFilter bookFilter){
        return this.bookService.bookFilter(bookFilter);
    }

    @PostMapping("")
    @ResponseBody
    public Map<String, Object> create(@RequestBody Book book){
        this.bookService.create(book);
        Map<String, Object> response = new HashMap<>();
        if (book != null) {
            response.put("Security", "Access Denied");
        }
        response.put("status",true);
        response.put("book", book);
        response.put("message", "Your book is inserted successfully :)");
        return response;
    }

    @DeleteMapping("/{id}")
    @ResponseBody
    public Map<String, Object> delete(@PathVariable("id") Integer id){
        this.bookService.delete(id);

        Map<String, Object> response = new HashMap<>();
        response.put("status",true);
        response.put("message","Your selected book has been deleted successfully :)");

        return response;
    }

    @GetMapping("/{id}")
    @ResponseBody
    public Map<String, Object> findOne(@PathVariable("id") Integer id){
        Book book = this.bookService.findOne(id);

        Map<String, Object> response = new HashMap<>();
        if (book != null){
            response.put("book",book);
            response.put("status",true);
            response.put("message","Book found!!!");
        }else{
            response.put("status",false);
            response.put("message","Book not found!!!");
        }
        return response;
    }

    @PutMapping("")
    @ResponseBody
    public Map<String, Object> update(@RequestBody Book book){
        this.bookService.update(book);

        Map<String,Object> response = new HashMap<>();
        response.put("status",true);
        response.put("book", book);
        response.put("message","Your selected book has been updated!!!");

        return response;
    }

    @PostMapping("/upload")
    @ResponseBody
    public Map<String, Object> upload(@RequestParam("file_btb") @RequestBody MultipartFile file){
        String filename = this.uploadService.upload(file);

        Map<String, Object> response = new HashMap<>();

        if (filename != null){
            response.put("status",true);
            response.put("file",file);
            response.put("message", "Upload success!!!");
        }else{
            response.put("status", false);
            response.put("message", "Upload failed!!!");
        }

        return response;
    }

    @PostMapping("/multi")
    @ResponseBody
    public Map<String, Object> creates(@RequestBody List<Book> books){
        this.bookService.creates(books);

        Map<String, Object> response = new HashMap<>();

        response.put("status",true);
        response.put("books", books);
        response.put("message","Done multiple upload!!!");

        return response;
    }
}
