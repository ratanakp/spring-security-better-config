package com.example.demo.controller.restcontroller;

import com.example.demo.models.Category;
import com.example.demo.service.CategoryService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/category")
public class CategoryRestController {

    private CategoryService categoryService;

    @Autowired
    public CategoryRestController(CategoryService categoryService){
        this.categoryService = categoryService;
    }

    @GetMapping
    @ApiOperation(value = "getAllCategory", authorizations = {@Authorization(value = "basicAuth")})
    public Map<String, Object> getAll(){
        Map<String, Object> response = new HashMap<>();

        List<Category> categories = this.categoryService.getAllCategory();

        if (categories != null){
            response.put("status",true);
            response.put("categories",categories);
            response.put("record_count",categories.size());
            response.put("message", "Categories Found!!!!");
        }else{
            response.put("status",true);
            response.put("message", "Categories not found!!!!");
        }
        return response;
    }

}
