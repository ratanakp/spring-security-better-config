package com.example.demo;

import com.example.demo.repositories.UserRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Base64;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(DemoApplication.class, args);

        UserRepository repository = context.getBean(UserRepository.class);

        System.out.println(repository.loadUserByUsername("ehan"));

        PasswordEncoder passwordEncoder = context.getBean("passwordEncoder", PasswordEncoder.class);

        String passwordEncoded = passwordEncoder.encode("ehan");
        System.out.println("Ehan: '" + passwordEncoded + "'");


        System.out.println(passwordEncoder.matches("ehan", "$2a$10$bO3Y3zY0LgVTKTFFTSWjv.mUKPqmLXovkoUhsTwFvnG9n8h.0KfrG"));

        System.out.println(Base64.getUrlEncoder().encodeToString("ehan:ehan".getBytes()));


    }
}
