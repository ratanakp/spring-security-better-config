package com.example.demo.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface UploadService {

    String singleFileUpload(MultipartFile file, String folder);

    List<String> multipleFileUpload(List<MultipartFile> files, String folder);

    String upload(MultipartFile file);
    String upload(MultipartFile file, String folder);

    List<String> upload(List<MultipartFile> files);
    List<String> upload(List<MultipartFile> files, String folder);
}
