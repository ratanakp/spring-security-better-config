package com.example.demo.service;

import com.example.demo.models.Book;
import com.example.demo.models.filter.BookFilter;

import java.util.List;

public interface BookService {
    List<Book> getBookList();
    Book findOne(int id);
    boolean update(Book book);
    boolean delete(int id);
    boolean create(Book book);
    boolean creates(List<Book> books);
    List<Book> bookFilter(BookFilter bookFilter);
}
