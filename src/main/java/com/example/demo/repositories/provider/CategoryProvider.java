package com.example.demo.repositories.provider;

import org.apache.ibatis.jdbc.SQL;

public class CategoryProvider {

    public String getBookByCateIdProvider(Integer id){
        return new SQL(){{
            SELECT("*");
            FROM("tb_books b");
            WHERE("b.cate_id=#{id}");
        }}.toString();
    }

}
