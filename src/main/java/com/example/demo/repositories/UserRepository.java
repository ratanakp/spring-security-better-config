package com.example.demo.repositories;

import com.example.demo.models.Role;
import com.example.demo.models.User;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface
UserRepository {

    @Select("SELECT * FROM tb_user WHERE username=#{username}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "profileImg", column = "profile_img"),
            @Result(property = "roles", column = "id", many = @Many(select = "getRolesByUserId"))
    })
    User loadUserByUsername(String username);

    @Select("SELECT * FROM tb_role tr\n" +
            "INNER JOIN tb_user_role tur ON tr.id = tur.role_id\n" +
            "WHERE tur.user_id = #{id}")
    List<Role> getRolesByUserId(Integer id);
}
