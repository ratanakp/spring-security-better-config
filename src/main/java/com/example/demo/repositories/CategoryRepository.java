package com.example.demo.repositories;

import com.example.demo.models.Book;
import com.example.demo.models.Category;
import com.example.demo.repositories.provider.CategoryProvider;
import com.example.demo.service.CategoryService;
import org.apache.ibatis.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {

    @Select("SELECT * FROM tb_category")
    @Results({
            @Result(column = "id", property = "id"),
            @Result(property = "books", column = "id", many = @Many(select = "getBookByCateId"))
    })
    List<Category> getAllCategory();

    @SelectProvider(type = CategoryProvider.class, method = "getBookByCateIdProvider")
    List<Book> getBookByCateId(Integer id);
}
