package com.example.demo.repositories;

import com.example.demo.models.Book;
import com.example.demo.models.filter.BookFilter;
import com.example.demo.repositories.provider.BookProvider;
import com.github.javafaker.Faker;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.StatementType;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface BookRepository {
    /** using @Select */
    // @Select("select * from tb_books order by id asc")
    /**select record from tb_books using @SelectProvider*/
    @SelectProvider(type = BookProvider.class,method = "selectBookListProvider")
    @Results({
            @Result(column = "cate_id", property = "category.id"),
            @Result(column = "name", property = "category.name")
    })
    List<Book> getBookList();


    @SelectProvider(type = BookProvider.class,method = "bookFilterProvider")
    @Results({
            @Result(column = "cate_id", property = "category.id"),
            @Result(column = "name", property = "category.name")
    })
    List<Book> bookFilter(BookFilter bookFilter);







    /** if the method parameter has more than one, we have to use @Param to specified
     *  the parameter that we want to select. */
    /*@Select("SELECT * FROM tb_books where id=#{id}")*/
    @SelectProvider(type=BookProvider.class, method="selectBookProvider")
    @Results({
            @Result(column = "cate_id", property = "category.id"),
            @Result(column = "name", property = "category.name")
    })
    Book findOne(@Param("id") Integer id);



    /*@Update("UPDATE tb_books SET title=#{title}, author=#{author}, publisher=#{publisher}, thumbnail=#{thumbnail}" +
            "WHERE id=#{id}")*/
    @UpdateProvider(type=BookProvider.class, method="updateBookProvider")
    boolean update(Book book);



    /*@Delete("DELETE FROM tb_books WHERE id=#{id}")*/
    @DeleteProvider(type = BookProvider.class, method="deleteBookProvider")
    boolean delete(Integer id);


    /*@Insert("INSERT INTO tb_books(title,author,publisher,thumbnail) VALUES(#{title},#{author},#{publisher},#{thumbnail})")*/
    @InsertProvider(type = BookProvider.class,method="insertBookProvider")
    @SelectKey(
            keyProperty = "id",
            keyColumn = "",
            statementType = StatementType.PREPARED,
            before = false,
            resultType = Integer.class,
            statement = "SELECT CURRVAL('tb_books_id_seq') AS curr_id"
    )
    boolean create(Book book);

    /* upload multiple books */
    @Insert({
            "<script>" ,
                "INSERT INTO tb_books(title,author,publisher, thumbnail, cate_id) VALUES" ,
                    "<foreach collection='books' item='book' index='ind' separator=','>(" ,
                        "#{book.title}" ,
                        ",#{book.author}" ,
                        ",#{book.publisher}" ,
                        ",#{book.thumbnail}",
                        ",#{book.category.id}",
                    ")</foreach>" ,
            "</script>"
    })
    boolean creates(@Param("books")List<Book> books);









    /*Faker faker = new Faker();

    List<Book> bookList = new ArrayList<>();

    {
        for (int i = 1; i <=10 ; i++) {
            Book book =  new Book();

            book.setId(i);
            book.setTitle(faker.book().title());
            book.setAuthor(faker.book().author());
            book.setPublisher(faker.book().publisher());

            bookList.add(book);
        }
    }

    public List<Book> getBookList(){
        return this.bookList;
    }

    public Book findOne(int id){
        for (int i = 0; i < bookList.size() ; i++) {
            if (bookList.get(i).getId() == id){
                return bookList.get(i);
            }
        }
        return null;
    }

    public boolean update(Book book){
        for (int i=0; i<bookList.size(); i++){
            if (bookList.get(i).getId() == book.getId()){
                bookList.set(i,book);
            }
        }
        return false;
    }

    public boolean delete(int id){
        for (int i = 0; i <bookList.size() ; i++) {
            if(bookList.get(i).getId() == id){
                bookList.remove(i);
                return true;
            }
        }
        return false;
    }

    public List<Book> create(Book book){
        this.bookList.add(book);
        return bookList;
    }*/

}
