package com.example.demo.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Configuration
public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint {

//    /* This method is working when user want to access the URI ( like /admin, /user, /dba) */
//


    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, org.springframework.security.core.AuthenticationException e) throws IOException, ServletException {
        System.out.println("Login required!");

        String requestURI = httpServletRequest.getRequestURI();
        httpServletRequest.getSession().setAttribute("REDIRECT_URI", requestURI);
        System.out.println("requestURI" + requestURI);
        httpServletResponse.sendRedirect("/login");
    }


}
