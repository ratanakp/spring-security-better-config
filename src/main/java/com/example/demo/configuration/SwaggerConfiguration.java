package com.example.demo.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;


@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    @Bean
    public Docket api(){
        List<SecurityScheme> securitySchemes = new ArrayList<>();
        securitySchemes.add(new BasicAuth("basicAuth"));
        securitySchemes.add(new ApiKey("API key","API Keyname", "API PASSWORD"));

        return new Docket(DocumentationType.SWAGGER_2)  /* select Document type as Swagger 2*/
                .select()   /* used to select to select api package */
                .apis(RequestHandlerSelectors.any())  /* this statement is used to select all packages */
                .apis(RequestHandlerSelectors.basePackage("com.example.demo.controller.restcontroller")) /* used to select specific package */
                .paths(PathSelectors.any()) /* used to select all path */
                .paths(PathSelectors.ant("/api/v1/**")) /* used to select specific path */
                .build()
                .securitySchemes(securitySchemes)
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo(){
        List<VendorExtension> vendorExtensions = new ArrayList<>();
//        vendorExtensions.add("");
        /* This class is used to provide API developer info to the API users */
        Contact contact = new Contact("Sothearoth","","sothearothphorn@gmail.com");
        /* Provide all API info to the users */
        ApiInfo apiInfo = new ApiInfo(
                "BTB-6th-BMS",
                "BMS API Documentation",
                "Version 1.swagger-ui.swagger-ui",
                "Term of service",
                contact,
                "License: copy right",
                "https://www.btbbms6th.com",
                vendorExtensions
        );
        return apiInfo;

    }


}
