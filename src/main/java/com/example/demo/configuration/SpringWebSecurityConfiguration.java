//package com.example.demo.configuration;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.annotation.Order;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.builders.WebSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.security.web.AuthenticationEntryPoint;
//import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
//import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
//
//@Order(2)
//@EnableWebSecurity
//public class SpringWebSecurityConfiguration extends WebSecurityConfigurerAdapter {
//
//    @Autowired
//    private AuthenticationSuccessHandler successHandler;
//
//    @Autowired
//    private AuthenticationEntryPoint authenticationEntryPoint;
//
//    @Autowired
//    @Qualifier("userDetailServiceImpl")
//    private UserDetailsService userDetailsService;
//
//    @Autowired
//    private PasswordEncoder passwordEncoder;
///*
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//
//        auth.inMemoryAuthentication().withUser("admin")
//                .password("{noop}admin").roles("ADMIN","DBA","USER")
//                .and()
//                .withUser("dba")
//                .password("{noop}dba").roles("DBA", "USER");
//        auth.inMemoryAuthentication().withUser("user")
//                .password("{noop}user").roles("USER");
//    }*/
//
//    @Override
//    public void configure(WebSecurity web) throws Exception {
//        web.ignoring().mvcMatchers("/static/**");
//    }
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        /*-- used to disable authenticated all URL in the application --*/
//        /*http.authorizeRequests().antMatchers("/").authenticated();*/
//
////        http.csrf().disable();
//
//        /* used to custom login form*/
//        http.formLogin()
//                .loginProcessingUrl("/login/submit")
//                .usernameParameter("username_btb")
//                .passwordParameter("password_btb")
//                .successHandler(successHandler)
//                .loginPage("/login");
//
//        /* used to set authentication for the URI below */
//        http.authorizeRequests().antMatchers("/admin/**").hasAnyRole("ADMIN");
//        http.authorizeRequests().antMatchers("/dba/**").hasAnyRole("DBA");
//        http.authorizeRequests().antMatchers("/user/**").hasAnyRole("USER");
//
//
////        http.logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout"));
//
//        http.logout().logoutSuccessUrl("/login")
//                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"));
//
//
//
//        http.exceptionHandling().accessDeniedPage("/accessdenied")
//            .authenticationEntryPoint(authenticationEntryPoint);
//
//        /* used to redirect to other page when session timeout */
////        http.sessionManagement().invalidSessionUrl("/index");
//
////        http.csrf().disable();
//    }
//}
