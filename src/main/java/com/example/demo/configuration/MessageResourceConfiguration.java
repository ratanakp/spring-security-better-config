package com.example.demo.configuration;


import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;

/** This class is used to configure message resource location and file name because by default spring
 *  itself it takes file name 'messages' as a default file name for message resource,
 *  so if we need to create a new message resource with different file name beside 'messages' and place it
 *  in others directory rather than the default one then we need to configure it like configuration code below. */

@Configuration
public class MessageResourceConfiguration {

    /** This bean is used to configure message resources*/
    @Bean
    public MessageSource messageSource(){
        ResourceBundleMessageSource resourceBundleMessageSource = new ResourceBundleMessageSource();

        /** The setBasenames() method below is used to make spring know the message resources file that have created
         *  with different name and located in other directory that is difference from the default one.
         *  This method can takes up as many parameter as we want.
         *  for example: resourceBundleMessageSource.setBasenames("i18n/languages", "messages", "message"); */
        resourceBundleMessageSource.setBasenames("i18n/languages");

        /** This setDefaultEncoding("param") is used to set default encoding for web browser in order to
         *  help it understand most languages that we've set like Khmer, Thai, ..., etc.
         *  for example: resourceBundle.setDefaultEncoding("UTF-8"*/
        resourceBundleMessageSource.setDefaultEncoding("UTF-8");

        return resourceBundleMessageSource;
    }


}
