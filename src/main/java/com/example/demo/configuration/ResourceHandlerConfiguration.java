package com.example.demo.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/** This class is used to configure resource handler to enable us to access to resource that we have uploaded
 *  by using the specified path*/

@Configuration
@PropertySource("classpath:/bms.properties")
public class ResourceHandlerConfiguration implements WebMvcConfigurer {

    @Value("${file.client.path}")
    private String CLIENT_PATH;
    @Value("${file.server.path}")
    private String SERVER_PATH;
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        /** The addResourceHandler() is used to create url to map to the resource that we have uploaded.
         *  noted: [ /image-btb/** ] mean that it can get all kind of resources in our server that we have mapped to.
         *  The addResourceLocations() is used to point to our resources location. */
        registry.addResourceHandler(CLIENT_PATH+"**").addResourceLocations("file:"+SERVER_PATH);

        registry.addResourceHandler("/resources/**").addResourceLocations("classpath:/static/");

        registry.addResourceHandler("/swagger-ui/**").addResourceLocations("classpath:/static/swagger-ui/");

        registry.addResourceHandler("/swaggerv2/**").addResourceLocations("classpath:/static/swagger-v2.1.3/dist/");
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/swagger-ui").setViewName("/swagger/index");

        registry.addViewController("/swaggerv2").setViewName("/swagger/V2.1.3/index");

        registry.addViewController("/login").setViewName("login-page");

        registry.addViewController("/accessdenied").setViewName("accessdeny-page");
    }
}
