package com.example.demo.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

@Configuration
public class DatasoureConfiguration {

    /* PostgreSQL datasource */
    @Bean("dataSource")
    @Profile("pgsql")
    public DataSource dataSource(){
        DriverManagerDataSource dataSource = new DriverManagerDataSource();

        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl("jdbc:postgresql://localhost:5432/db_spring_mybatis");
        dataSource.setUsername("postgres");
        dataSource.setPassword("admin123");

        return dataSource;
    }

    /* H2 Database memory datasource */
    @Bean("dataSource")
    @Profile("h2")
    public DataSource inMemoryDB(){
        EmbeddedDatabaseBuilder databaseBuilder = new EmbeddedDatabaseBuilder();

        databaseBuilder.addScript("db/schema.sql");
        databaseBuilder.addScript("db/data.sql");
        databaseBuilder.setType(EmbeddedDatabaseType.H2);

        return databaseBuilder.build();
    }

}
