package com.example.demo.configuration;

import com.zaxxer.hikari.util.SuspendResumeLock;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Configuration
public class CustomAuthenticationSuccessHandler  implements AuthenticationSuccessHandler {

    /* This method is working after the user login successfully. */

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {

        /* used to set session timeout in number of second */
        HttpSession httpSession = httpServletRequest.getSession();
        httpSession.setMaxInactiveInterval(30000);




        System.out.println("Jab Ban: " + httpServletRequest.getSession().getAttribute("REDIRECT_URI"));


        String redirectURI = (String) httpServletRequest.getSession().getAttribute("REDIRECT_URI");
        System.out.println(redirectURI);
        if (redirectURI == null)
            redirectURI = "/";

        System.out.println(authentication.getPrincipal());


//        for (GrantedAuthority grantedAuthority:
//                authentication.getAuthorities()) {
//
//            System.out.println(grantedAuthority.getAuthority());
//
//            System.out.println("==============getDetails================");
//            System.out.println(authentication.getDetails());
//            System.out.println("==============getPrincipal================");
//            System.out.println(authentication.getPrincipal());
//            System.out.println("==============getCredentials================");
//            System.out.println(authentication.getCredentials());
//            System.out.println("===============getAuthorities===============");
//            System.out.println(authentication.getAuthorities());
//            System.out.println("===============getName===============");
//            System.out.println(authentication.getName());
//
//
//            if (grantedAuthority.getAuthority().equals("ROLE_ADMIN")) {
//                redirectURI = "/admin";
//                break;
//            }
//            else if (grantedAuthority.getAuthority().equals("ROLE_DBA")) {
//                redirectURI = "/dba";
//                break;
//            }
//            else if (grantedAuthority.getAuthority().equals("ROLE_USER")) {
//                redirectURI = "/user";
//                break;
//            }
//
//        }

        httpServletResponse.sendRedirect(redirectURI);
    }
}
