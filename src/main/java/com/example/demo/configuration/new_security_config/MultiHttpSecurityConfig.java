package com.example.demo.configuration.new_security_config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@EnableWebSecurity
@Configuration
public class MultiHttpSecurityConfig {

    private PasswordEncoder passwordEncoder;

    public MultiHttpSecurityConfig(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    //region Create In Memory User using User Builder
    /*
    *
    *TODO: Note --> Can not use at the same time with Database

    */
    /*@Bean
    public UserDetailsService userDetailsService() throws Exception {
        // ensure the passwords are encoded properly
        User.UserBuilder users = User.builder();
        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
        manager.createUser(users.username("user").password(passwordEncoder.encode("user")).roles("USER").build());
        manager.createUser(users.username("admin").password(passwordEncoder.encode("admin")).roles("USER", "ADMIN").build());

        return manager;
    }*/

   /* @Bean
    public InMemoryUserDetailsManager inMemoryUserDetailsManager() {
        User.UserBuilder users = User.builder();
        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
        manager.createUser(users.username("user").password(passwordEncoder.encode("user")).roles("USER").build());
        manager.createUser(users.username("ehan").password(passwordEncoder.encode("ehan")).roles("USER", "ADMIN").build());

        return manager;

    }*/
    //endregion

    @Configuration
    @Order(1)
    public static class ApiWebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {
        protected void configure(HttpSecurity http) throws Exception {

            http
                    .antMatcher("/api/v1/**")
                    .authorizeRequests()
                    .anyRequest().hasRole("ADMIN")
                    .and()
                    .httpBasic();

            http.csrf().disable();

            http
                    .sessionManagement().
                    sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        }
    }

    @Configuration
    public static class FormLoginWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {


        private AuthenticationEntryPoint authenticationEntryPoint;
        private AuthenticationSuccessHandler successHandler;

        public FormLoginWebSecurityConfigurerAdapter(
                AuthenticationEntryPoint authenticationEntryPoint,
                AuthenticationSuccessHandler successHandler) {
            this.authenticationEntryPoint = authenticationEntryPoint;
            this.successHandler = successHandler;
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {

            // All Request Must login, but any role can login
            /*http
                    .authorizeRequests()
                    .anyRequest().authenticated();*/

            http.authorizeRequests()
                    .antMatchers("/swagger-ui/**", "/swaggerv2/**")
                    .hasRole("ADMIN");

            http
                    .formLogin()
                    .loginPage("/login")
                    .loginProcessingUrl("/login/submit")
                    .usernameParameter("username_btb")
                    .passwordParameter("password_btb")
                    .successHandler(successHandler)
//                    .defaultSuccessUrl("/swagger-ui")
                    .permitAll();


            http.logout().logoutSuccessUrl("/login")
                    .logoutRequestMatcher(new AntPathRequestMatcher("/logout"));


//            http.exceptionHandling().accessDeniedPage("/access-denied-page");

            http.exceptionHandling().authenticationEntryPoint(authenticationEntryPoint);
        }
    }
}
