//package com.example.demo.configuration;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.annotation.Order;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.config.http.SessionCreationPolicy;
//import org.springframework.security.crypto.password.PasswordEncoder;
//
//import java.util.Base64;
//
//@Order(1)
//@EnableWebSecurity
//public class ApiSecurityConfiguration extends WebSecurityConfigurerAdapter {
//
//    private PasswordEncoder passwordEncoder;
//
//    public ApiSecurityConfiguration(PasswordEncoder passwordEncoder){
//        this.passwordEncoder = passwordEncoder;
//    }
//
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication().withUser("api")
//                                    .password(passwordEncoder.encode("api")).roles("API");
//    }
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http.antMatcher("/api/v1/**").authorizeRequests()
//                                .anyRequest()
//                                .hasAnyRole(" API");
//
//
//        http.httpBasic();
//
//        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
//    }
//
//    public static void main(String[] args) {
//        String username = "api1";
//        String password = "api1";
//
//        String basicKey = Base64.getUrlEncoder().encodeToString((username+":"+password).getBytes());
//        System.out.println(basicKey);
//    }
//
//}
