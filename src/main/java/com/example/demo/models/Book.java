package com.example.demo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ApiModel("Book Model")  /* used to change model name in API Doc*/
public class Book {

    /** validate each field using annotation
     *  @Notnull, @Size, @NotEmpty, ... */

//    @NotNull(message = "Must not be null")
//    @JsonIgnore        /* we used it if we don't want to display it */
    private Integer id;

    @Size(min = 5, max = 255, message = "Size must be between 5 to 255 character!")
    @NotEmpty(message = "This field must not be empty!")
//    @JsonProperty("book_title")
    private String title;

    @Size(min = 5, max = 255, message = "Size must be between 5 to 255 character!")
    @NotEmpty(message = "This field must not be empty!")
    private String author;

    @Size(min = 5, max = 255, message = "Size must be between 5 to 255 character!")
    @NotEmpty(message = "This field must not be empty!")
    private String publisher;


    private String thumbnail;

    private Category category;

    public Book(){}

    public Book(@NotNull(message = "Must not null") Integer id, @Size(min = 5, max = 255, message = "Size must be between 5 to 255 character!") @NotEmpty(message = "This field must not be empty!") String title, @Size(min = 5, max = 255, message = "Size must be between 5 to 255 character!") @NotEmpty(message = "This field must not be empty!") String author, @Size(min = 5, max = 255, message = "Size must be between 5 to 255 character!") @NotEmpty(message = "This field must not be empty!") String publisher, String thumbnail, Category category) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.thumbnail = thumbnail;
        this.category = category;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", publisher='" + publisher + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                ", category=" + category +
                '}';
    }
}
