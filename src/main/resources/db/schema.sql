CREATE TABLE tb_category(
  id SERIAL PRIMARY KEY,
  name VARCHAR
);

CREATE TABLE tb_books(
  id SERIAL PRIMARY KEY,
  title VARCHAR ,
  author VARCHAR ,
  publisher VARCHAR ,
  thumbnail VARCHAR,
  cate_id INT REFERENCES tb_category(id)
);

create table tb_role (
  id   serial primary key,
  role varchar
);

create table tb_user (
  id          serial primary key,
  username    varchar,
  password    varchar,
  status      boolean default true,
  profile_img varchar
);

create table tb_user_role (
  user_id int not null
    constraint user_id_fk references tb_user (id) on delete cascade on update cascade,
  role_id int not null
    constraint role_id_fk references tb_role (id) on delete cascade on update cascade,

  primary key (user_id, role_id)
);
