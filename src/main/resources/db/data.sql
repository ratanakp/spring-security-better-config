INSERT INTO tb_category(name)
VALUES('Science fiction');

INSERT INTO tb_category(name)
VALUES('Drama');

INSERT INTO tb_category(name)
VALUES('Mystery');

INSERT INTO tb_category(name)
VALUES('Comics');

INSERT INTO tb_category(name)
VALUES('Fantasy');

INSERT INTO tb_books(title,author,publisher, cate_id)
VALUES('Java','Jame','Jack',1);

INSERT INTO tb_books(title,author,publisher, cate_id)
VALUES('HTML','Jake','Jack', 2);

INSERT INTO tb_books(title,author,publisher, cate_id)
VALUES('CSS','Net','Jack', 3);

INSERT INTO tb_books(title,author,publisher, cate_id)
VALUES('Kotlin','John','Jack', 2);

INSERT INTO tb_books(title,author,publisher, cate_id)
VALUES('Node.JS','Kale','Jack', 3);


insert into tb_role (role) values ('ADMIN'), ('DBA'), ('USER');


insert into tb_user (username, password, profile_img) values
  ('ehan', 'ehan', 'ehan.png'),
  ('piseth', 'piseth', 'piseth.png'),
  ('thearoth', 'thearoth', 'thearoth.png');


insert into tb_user_role (user_id, role_id) values (1, 1), (1, 3), (2, 2), (2, 3), (3, 1), (3, 2), (3, 3);

update tb_user
set password = '$2a$10$bO3Y3zY0LgVTKTFFTSWjv.mUKPqmLXovkoUhsTwFvnG9n8h.0KfrG'
where id = 1;