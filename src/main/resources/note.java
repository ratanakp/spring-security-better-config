
-------------------------------------------- MyBatis ------------------------------------------------------
---> To work with MyBatis we need to configure datasource and we can configure it in two way:
        1. configure in application.properties file
        2. Configure in Java class

-------------------------------------------- End of MyBatis ------------------------------------------------

--------------------------------- Swagger API Document Customization ---------------------------------------

/* used to set custom status code message*/
@ApiResponses({
        @ApiResponse(code = 200, message = "Ok custom"),
        @ApiResponse(code = 404, message = "Not found custom")
})

/* used to change model name in API Doc*/
@ApiModel("Book Model")

/* used to hide API in API Doc*/
@ApiIgnore

/* used to add description to API in API Doc */
@ApiOperation(value = "Get all books")

/* used to customize API Doc title for each API*/
@Api(description = "Here is my Book Web Service")

-------------------------------- End of Swagger API Document Customization ---------------------------------


------------------------------------ Thymeleaf Extras SpringSecurity ---------------------------------------

To use Thymeleaf Extras SpringSecurity features we need to add its dependency and namespace:
    -dependency
        <!-- https://mvnrepository.com/artifact/org.thymeleaf.extras/thymeleaf-extras-springsecurity4 -->
        <dependency>
        <groupId>org.thymeleaf.extras</groupId>
        <artifactId>thymeleaf-extras-springsecurity4</artifactId>
        <version>3.0.2.RELEASE</version>
        </dependency>
    -namespace
        xmlns:sec="http://www.thymeleaf.org/extras/spring-security"

    --> Below is some example of using it:

        <!-- first way to catch username -->
        <h3 sec:authorize="isAuthenticated()" th:text="${#authentication.principal.username}" class="text-light">Account</h3>-->

        <!-- second way to catch username -->
        <h3 sec:authorize="isAuthenticated()" sec:authentication="name" class="text-light"></h3>

        <h3 sec:authorize="isAnonymous()">Account</h3>

        <div sec:authorize="hasRole('ROLE_Admin')">
                This is for Admin
        </div>

    --> Link for Thymeleaf security document
        -https://github.com/thymeleaf/thymeleaf-extras-springsecurity

------------------------------- End of Thymeleaf Extras SpringSecurity --------------------------------------

